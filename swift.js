
// window.debug = true;
window.onload = function() {
  function word(word, duration, wait) {
    this.word = word;
    this.duration = duration;
    this.wait = wait;

    this.canvas_draw = function (ctx) {
      this.ctx = ctx;

      var kern = 1;
      var x = 100;
      var idx = Math.min(Math.floor(0.33 * this.word.length)+1, 4);

      ctx.beginPath();

      ctx.font = "normal 30px arial";

      var y = 60;

      for(var i = 0; i < idx; ++i){
        x -= ctx.measureText(this.word.charAt(i)).width + kern;
      }

      x -= ctx.measureText(this.word.charAt(idx)).width/2;

      for(var i = 0; i <= this.word.length; ++i){
          var ch = this.word.charAt(i);

          if (idx == i)
            ctx.fillStyle = 'red';
          else
            ctx.fillStyle = 'black';

          ctx.fillText(ch, x, y);
          x += ctx.measureText(ch).width + kern;
      }
      ctx.stroke();
    }

    this.canvas_clear = function () {
      this.ctx.clearRect(0,25,500,50);
    }

    this.clear = function(next) {
      this.canvas_clear();

      if (window.debug)
        console.log("Clear " + this.word)      // clear canvas
      setTimeout(function() { next(); }, this.wait);
    }

    this.show = function(canvas, next) {
      var pedant = this;
      this.canvas_draw(canvas);

      if (window.debug)
        console.log("Draw " + this.word);
      setTimeout(function() { pedant.clear(next); }, this.duration);
    }
  }

  function word_smith(rate) {
    this.rate = rate || 400; //wpm

    if (this.rate) {
      console.log("(word_smith) Set Rate: " + this.rate)
      this.char_multiplier = 29000 / this.rate;
      this.whitespace_delay = 500 / this.rate;
    }

    this.smith = function (_word) {
      return new word(_word,
                  this.calc_display_time(_word),
                  this.calc_pause_time(_word));
    }

    this.calc_modifiers = function(_word) {
      if (_word.match(/[.,;?!$()]/))
        return 200;
      return 0;
    }

    this.calc_display_time = function (_word) {
      if (window.debug)
        console.log(Math.log(_word.length) * this.char_multiplier)

      return Math.max(Math.log(_word.length)/Math.log(2) * this.char_multiplier, 120);
    }

    this.calc_pause_time = function (_word) {
      var modifier = this.calc_modifiers(_word);
      return this.whitespace_delay + modifier;
    }
  }

  function swift() {
    this.canvas = document.getElementById('swift');
    this.ctx = this.canvas.getContext('2d');
    this.ctx.setTransform(1, 0, 0, 1, 0, 0);
    this.ctx.scale(4,4)

    this.ctx.beginPath();
      this.ctx.fillStyle = 'black 2px';
      this.ctx.moveTo(100,0);
      this.ctx.lineTo(100,25);
      this.ctx.moveTo(101,0);
      this.ctx.lineTo(101,25);
      this.ctx.moveTo(100,100);
      this.ctx.lineTo(100,75);
      this.ctx.moveTo(101,100);
      this.ctx.lineTo(101,75);
    this.ctx.stroke();

    this.content = '';
    this.anvil = new word_smith();
    this.offset = 0;
    this.run = false;

    this.set_rate = function(rate) {
      this.rate = rate;
      console.log("Setting rate: " + rate);
      this.anvil = new word_smith(rate);
      this.load_content(this.content);
    }

    this.load_content = function(content) {
      this.content = content;
      this.offset = 0;
      
      var display = content.split(/[ ,]+/);
      var length = display.length;
      var timems = 0;

      this.words = []
      for (var x=0; x<length; x++) {
        var smithed = this.anvil.smith(display[x]);
        timems += smithed.duration + smithed.wait;
        this.words.push(smithed);
      }

      timems /= 1000;
      document.getElementById('smithed').innerHTML = "Total Time: " + timems + "s";
      document.getElementById('words').innerHTML = "Words: " + this.words.length;

      timems /= 60;
      document.getElementById('wpm').innerHTML = "WPM: " + this.words.length / timems;
    }

    this.next = function() {
      var pedant = this;

      if (window.debug)
        console.log("Nurxt" + this.words.length);

      if (!this.run || this.offset >= this.words.length)
        return;

      this.words[this.offset].show(this.ctx, function() { pedant.next() });
      this.offset++;
    }

    this.start = function() {
      this.run = true;
      this.next();
    }

    this.stop = function() {
      this.run = false;
    }
  }

  window.swift = new swift();
  window.swift.load_content('Zakalwe, in all human societies we have ever reviewed, in every age and every state, there has seldom if ever been a shortage of eager young males prepared to kill and die to preserve the security, comfort and prejudices of their elders, and what you call heroism is just an expression of this simple fact; there is never a scarcity of idiots');

  window.reset_swift = function() {
    window.swift.load_content(document.getElementById('text').value);
  }
};




