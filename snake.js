var id = 0;

function Field(width, height) {
  this.id = id++;
  this.canvas = document.getElementById('snake');
  this.speed = 100;

  if (this.canvas.getContext)
    this.ctx = this.canvas.getContext('2d');

  this.reset = function() {
    this.last_tick = -1;
    this.time_start = (new Date()).getTime();
  }

  this.init = function(width, height) {
    this.canvas.width = width;
    this.canvas.height = height;
    this.field_width = width;
    this.field_height = height;

    this.actors = [];
    this.treats = [];

    this.reset();
  }

  this.add_treat = function(treat) {
    this.treats.push(treat);
    this.add_actor(treat);
  }

  this.tick = function () {
    // update
    var time = (new Date()).getTime() - this.time_start;

    time = Math.round(time / this.speed);

    var context = this.ctx;
    var field = this;

    if (this.last_tick != time) {
      this.treats.forEach(function(treat) {
        if (field.player.collides_with(treat)) {
          field.player.eat_treat(treat);
        }
      })

      update_elapsed(time);

      this.last_tick = time;

      this.ctx.clearRect(1, 1, this.field_width-2, this.field_height-2);

      context.beginPath();
      context.strokeStyle = 'black';
      context.rect(0, 0, width, height);
      context.stroke();

      this.actors.forEach(function (actor, idx) {
        actor.tick(field);
        context.beginPath();

        actor.render(context);

        if (actor.done()) {
          field.remove_actor(actor)
        }
      });
    }

    requestAnimFrame(function() {
      field.tick(this.time_start);
    });
  }

  this.add_actor = function (actor) {
    this.actors.push(actor);
  }

  this.set_player = function (player) {
    this.actors.push(player);
    this.player = player;
  }

  this.remove_actor = function (actor) {
    var index = this.actors.indexOf(actor);
    this.actors.splice(index, 1);

    var index = this.treats.indexOf(actor);
    if (index > -1)
      this.treats.splice(index, 1);
  }

  this.roll_position = function (pos) {
    if (pos.x >= this.field_width)
      pos.x = 0;
    if (pos.y >= this.field_height)
      pos.y = 0;
    if (pos.x < 0)
      pos.x = this.field_width-1;
    if (pos.y < 0)
      pos.y = this.field_height-1;

    return pos;
  }

  this.init(width, height);
}
 
function Collider(x, y, size) {
  this.id = id++;
  this.pos = { x: 0, y: 0 }
  this.size = size
}

Collider.prototype.collides = function(col) {
  var ax = col.pos.x + col.size/2;
  var ay = col.pos.y + col.size/2;

  var bx = this.pos.x + this.size/2;
  var by = this.pos.y + this.size/2;

  var dist = Math.sqrt((bx-ax)*(bx-ax) + (by-ay)*(by-ay));
  return (dist < (this.size+col.size)/2);
}

function Treat(field, ls, points) {
  this.id = id++;
  this.pos = { x: field.field_width * Math.random(),
               y: field.field_height * Math.random() };

  this.size = 10;
  this.life_span = field.last_tick + ls;
  this.points = points;
  this.finished = false;

  this.eat = function() {
    field.add_actor(new Explosion(this.pos, this.size, this.size*2, 3))
    this.finished = true;
  }

  this.render = function(ctx) {
    ctx.strokeStyle = 'green';
    ctx.rect(this.pos.x, this.pos.y, this.size, this.size);
    ctx.stroke();
  }

  this.tick = function(field) {
    if (field.player && !this.finished)
      this.finished = (field.last_tick >= this.life_span);
  }

  this.done = function() {
    return this.finished;
  }
}

// Inherit from the Collider class.
Treat.prototype = new Collider;

function TreatMaker(field) {
  this.id = id++;
  this.field = field;
  this.next_treat = 0;

  this.render = function(ctx) {}

  this.tick = function(field) {
    if (field.last_tick > this.next_treat) {
      var treat = new Treat(field, 100, 10);
      field.add_treat(treat);
      this.next_treat += Math.random() * 20 + 105;
    }
  }

  this.done = function() { return false; }
}

function Segment(parent) {
  this.id = id++;
  this.pos = { x: 0, y: 0 };
  this.size = parent.size;

  this.render = function(ctx) {
    ctx.strokeStyle = 'blue';
    ctx.rect(this.pos.x, this.pos.y, this.size, this.size);
    ctx.stroke();
  }
}

// Inherit from the collider class
Segment.prototype = new Collider;

function Explosion(pos, size_in, size_out, time) {
  this.id = id++;
  this.pos = pos;
  this.delta = (size_out - size_in)/time;
  this.size = size_in;
  this.time_to_die = time;
  this.life_span = time;
  this.dead = false;

  this.tick = function(field) {
    // Is this the first tick?
    if (this.life_span == this.time_to_die)
      // then set when we plan on disappearing
      this.time_to_die = field.last_tick + this.life_span;

    // update the size and position based on the requested values
    this.size += this.delta/2;
    this.pos.x -= this.delta/2;
    this.pos.y -= this.delta/2;

    // mark as dead if its passed its time-to-live (TTL)
    if (field.last_tick >= this.time_to_die)
      this.dead = true;
  }

  this.render = function(ctx) {
    // Set the color, render the rectangle, and stroke the brush.
    ctx.strokeStyle = 'red';
    ctx.rect(this.pos.x, this.pos.y, this.size, this.size);
    ctx.stroke();
  }

  this.done = function() {
    return this.dead;
  }
}

function Snake(field) {
  this.id = id++;
  //   1
  // 4   2
  //   3
  this.vel = 2;
  this.dir = 2;
  this.size = 10;
  this.speed = 10;
  this.grow_every = 20;
  this.grow_at = 0;
  this.pos = { x: 2, y: field.field_height/2 };
  this.field = field;
  this.pos_tick = -2;
  this.die = false;
  this.score = 0;

  this.cells = [];

  this.eat_treat = function(treat) {
    treat.eat();
    this.score += treat.points;
    update_score(this.score);
  }

  this.tick = function(field) {
    var segment = null;

    if (field.last_tick > this.grow_at) {
      this.grow_at += this.grow_every;

      segment = new Segment(this);
    } else {
      segment = this.cells.pop();
    }

    segment.pos = this.next_position();

    if (this.collides_with(segment)) {
      this.die = true;

      this.cells.forEach(function (seg) {
        var pos = { x: seg.pos.x, y: seg.pos.y }
        field.add_actor(new Explosion(pos, seg.size, seg.size*2, 3))
      });
    }

    this.cells.unshift(segment);

    if (this.next_dir > 0) {
      this.dir = this.next_dir;
      this.next_dir = 0;
    }
  };

  this.collides_with = function(collider) {      
    var collision = false;

    this.cells.forEach(function (seg) {
      collision = collision || collider.collides(seg);
    });

    return collision
  };

  this.render = function(ctx) {
    this.cells.forEach(function(segment) {
      segment.render(ctx);
    })
  };

  this.done = function() { return this.die; };

  this.set_dir = function (dir) {
    if (this.pos_tick == this.field.last_tick) {
      this.next_dir = dir;
      return;
    }

    this.dir = dir;
    this.pos_tick = this.field.last_tick;
  }

  this.left = function() {
    if (this.dir % 2 == 1)
      this.set_dir(4);
  };

  this.right = function() {
    if (this.dir % 2 == 1)
      this.set_dir(2);
  };

  this.up = function() {
    if (this.dir % 2 == 0)
      this.set_dir(1);
  };

  this.down = function() {
    if (this.dir % 2 == 0)
      this.set_dir(3);
  };

  this.next_position = function() {
    switch(this.dir) {
      case 1:
        this.pos.y -= this.speed;
        break;
      case 2:
        this.pos.x += this.speed;
        break;
      case 3:
        this.pos.y += this.speed;
        break;
      case 4:
        this.pos.x -= this.speed;
        break;
    }

    this.pos = this.field.roll_position(this.pos);
    return { x: this.pos.x, y: this.pos.y };
  }

  var segment = null;

  for (var x=0; x<5; x++) {
    segment = new Segment(this);
    segment.pos = this.next_position();
    this.cells.unshift(segment);
  }
}

window.requestAnimFrame = (function(callback) {
  return window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function(callback) {
      window.setTimeout(callback, 1000 / 60);
    };
})();

var field = null;
var player = null;

var reset_game = function() {
  if (field == null) {
    field = new Field(400, 400);
    field.add_actor(new TreatMaker(field));
  } else {
    field.reset();
  }

  if (player != null) {
    field.remove_actor(player);
  }

  player = new Snake(field);

  window.addEventListener('keydown', function(event) {
    switch (event.keyCode) {
      case 37: // Left
        player.left();
      break;

      case 38: // Up
        player.up();
      break;

      case 39: // Right
        player.right();
      break;

      case 40: // Down
        player.down();
      break;
    }

    if (event.keyCode == 82) {
      reset_game();
    }
  }, false);

  field.set_player(player);
}

window.update_score = function(score) {
  document.getElementById('score').innerHTML = score.toString();
}

window.update_elapsed = function(time) {
  time /= 10;
  document.getElementById('elapsed').innerHTML = time.toString();
}

var start = function () {
  setTimeout(function() {
    field.tick();
  }, 500);
}

window.onload = function() {
  reset_game();
  start();
}